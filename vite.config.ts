import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react()],
  server: {
    proxy: {
      "/api": {
        target: "http://localhost:3000/",
        changeOrigin: true,
        rewrite: (path) => path.replace(/^\/api/, ""),
      },
    },
  },
  build: {
    chunkSizeWarningLimit: 500,
    rollupOptions: {
      output: {
        manualChunks(id) {
          const url = new URL(id, import.meta.url);
          const chunkName = url.searchParams.get("chunkName");
          if (chunkName) {
            return chunkName;
          }
          if (id.search("primereact") > 0) {
            return "prime"
          }
          if (id.search("react") > 0) {
            return "react"
          }
          if (id.search("axios") > 0) {
            return "dev"
          }
          if (id.search("zod") > 0) {
            return "dev"
          }
        }
      }
    }
  }
})
