```
npm install -D tailwindcss postcss autoprefixer
npx tailwindcss init -p
npm install primereact
npm install primeicons
```

## tailwind.config.js
```
/** @type {import('tailwindcss').Config} */
export default {
    darkMode: 'class',
    content: [
        './index.html',
        './src/**/*.{vue,js,ts,jsx,tsx}',
        './node_modules/primereact/**/*.{js,ts,jsx,tsx}',
    ],
    theme: {
        extend: {},
    },
    plugins: [],
};
```

## index.css
```
@layer tailwind-base, primereact, tailwind-utilities;

/* IMPORTANT: In "styled" mode you must add the PrimeReact Theme here. Do NOT include in "unstyled" mode */
@import 'primereact/resources/themes/arya-green/theme.css' layer(primereact);
@import 'primeicons/primeicons.css' layer(primereact); 
        
@layer tailwind-base {
  @tailwind base;
}

@layer tailwind-utilities {
  @tailwind components;
  @tailwind utilities;
}
```

## postcss.config.js
```
export default {
    plugins: {
        tailwindcss: {},
        autoprefixer: {},
    },
}
```

## react router
```
npm install react-router-dom
```

## form and validator
```
npm install react-hook-form
npm install zod
npm install @hookform/resolvers
```

## REST Api
```
npm install axios
```

## State Management
```
npm install @reduxjs/toolkit react-redux
npm install -D @types/react-redux 
```

## msw (mock server)
```
npm install msw@latest --save-dev
npx msw init public
```

```
uat-hrms.aioibkking.co.th                -> hrms-web
uat-cmionline.aioibkking.co.th           -> cmionline-web
uat-api.aioibkking.co.th/hrms            -> hrms-api
uat-api.aioibkking.co.th/cmionline       -> cmionline-api

```