import { createBrowserRouter } from 'react-router-dom';
import { Login } from './components/Login';
import { Template } from './components/Template';
import { Home } from './components/Home';

export const router = createBrowserRouter(
    [
        {
            path: '',
            element: <Login />
        },
        {
            path: '',
            element: <Template />,
            children: [{
                path: 'home',
                element: <Home />
            }]
        }
    ]
);