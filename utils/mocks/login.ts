import { http, HttpResponse } from 'msw'

export const loginHandlers = [
  http.post("/api/login", async ({ request }) => {
    const body = await request.clone().json();
    if (body.userName === 'admin') {
      return HttpResponse.json({
        success: true,
        userName: "admin",
        firstName: "Administrator",
        lastName: "XX",
        token: ""
      });
    }
    if (body.userName === 'error') {
      return HttpResponse.error();
    }
    return HttpResponse.json({
      success: false,
      message: "Invalid username or password"
    });
  }),
]