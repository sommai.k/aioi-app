import { FC, useRef } from 'react';
import { FloatLabel } from 'primereact/floatlabel';
import { InputText } from 'primereact/inputtext';
import { Button } from 'primereact/button';
import cartoon from '../assets/cartoon.svg';
import { z } from 'zod';
import { zodResolver } from '@hookform/resolvers/zod';
import { useForm, SubmitHandler } from 'react-hook-form';
import { Toast } from 'primereact/toast';
import { api } from '../app/api';

// declare
const LoginSchema = z.object({
    userName: z.string({ message: "Username is required" }).min(3, "Username min length = 3").max(20, "Username max length = 20"),
    password: z.string({ message: "Password is required" }).min(8, "Password min length = 8").max(50, "Password max length = 50"),
});

type LoginModel = z.infer<typeof LoginSchema>;

export const Login: FC = () => {
    // hook
    const { register, handleSubmit, getFieldState } = useForm<LoginModel>({
        resolver: zodResolver(LoginSchema)
    });

    const toast = useRef<Toast>(null);

    // when login press
    const whenLogin: SubmitHandler<LoginModel> = async (loginForm) => {
        try {
            const resp = await api.post('/api/login', loginForm);
            if (resp.status === 200) {
                const jsonData = resp.data;
                if (jsonData['success']) {
                    // goto home page
                    toast.current!.show({
                        severity: 'info',
                        summary: 'Success',
                        detail: 'Login success',
                        sticky: true,
                    });
                } else {
                    // show warning invalid user
                    toast.current!.show({
                        severity: 'warn',
                        summary: 'Warning',
                        detail: 'Login fail',
                        sticky: true,
                    });
                }
            } else {
                // show server error
                toast.current!.show({
                    severity: 'error',
                    summary: 'Error',
                    detail: 'Server error',
                    sticky: true,
                });
            }
        } catch (e) {
            // show server error
            toast.current!.show({
                severity: 'error',
                summary: 'Error',
                detail: 'Server error',
                sticky: true,
            });
        }
    }
    // render
    return <>
        <div className='flex w-screen h-full bg-[url("bg-2.jpg")] bg-no-repeat bg-cover'>
            <Toast ref={toast} position='center' />
            <div className='flex flex-row w-full'>
                <div className='hidden lg:flex lg:w-1/2 bg-[#0066CC] flex-col items-center'>
                    <img src="hrem-logo-white.png" className='max-w-[290px] mt-12' />
                    <img src={cartoon} className='w-full' />
                </div>
                <div className='w-full lg:w-1/2 min-h-screen relative'>
                    <div className='p-8 lg:py-20 flex flex-col items-center space-y-10'>
                        <img src="logo.png" className='max-w-[290px]' />
                        <img src="hrem-logo-blue.png" className='visible lg:hidden max-w-[290px] mt-12' />
                        <div className='flex flex-row space-x-2 w-full'>
                            <span className='text-2xl font-bold text-slate-500'>Welcome to</span>
                            <span className='text-2xl font-bold text-[#0066CC]'>Login</span>
                        </div>
                        <div className='w-full'>
                            <FloatLabel>
                                <InputText
                                    id="username"
                                    className='w-full rounded-2xl'
                                    {...register("userName")}
                                    invalid={getFieldState('userName').invalid}
                                />
                                <label htmlFor="username">Username</label>
                            </FloatLabel>
                        </div>
                        <div className='w-full'>
                            <FloatLabel>
                                <InputText
                                    {...register("password")}
                                    id="password"
                                    type='password'
                                    className='w-full rounded-2xl'
                                    invalid={getFieldState('password').invalid}
                                />
                                <label htmlFor="password">Password</label>
                            </FloatLabel>
                        </div>
                        <Button label="LOGIN" className='w-full bg-[#0066CC]' rounded onClick={handleSubmit(whenLogin)} />
                    </div>
                    <div className='absolute bottom-5 text-center w-full text-gray-500'>©2024 บริษัท ไอโออิ กรุงเทพ ประกันภัย จำกัด (มหาชน)</div>
                </div>
            </div>
        </div>
    </>
}
