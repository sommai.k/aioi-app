import React from 'react'
import ReactDOM from 'react-dom/client'
import { PrimeReactProvider, addLocale } from 'primereact/api';
import { RouterProvider } from 'react-router-dom';
import { Provider } from 'react-redux';
import { setupWorker } from 'msw/browser';

import "./index.css";
import local from './assets/local.json';
import { router } from './router.tsx';
import { store } from './app/store.ts';
import { handlers } from '../utils/mocks';

// setup worker
const worker = setupWorker(...handlers);
worker.start();
// worker.use(...handlers);

// add local
addLocale('en', local.en);
addLocale('th', local.th);

// render
ReactDOM.createRoot(document.getElementById('root')!).render(
  <React.StrictMode>
    <Provider store={store}>
      <PrimeReactProvider value={{ unstyled: false }}>
        <RouterProvider router={router}></RouterProvider>
      </PrimeReactProvider>
    </Provider>
  </React.StrictMode>,
)
