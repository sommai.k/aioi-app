FROM  node:20.14.0-alpine AS build
WORKDIR /app
COPY . .
RUN npm i
RUN npm run build

FROM nginx:alpine
ENV API_URL=http://localhost:3000
RUN mkdir /etc/nginx/templates
COPY ./nginx /etc/nginx/templates
COPY --from=build /app/dist /usr/share/nginx/html
EXPOSE 80