import { FC } from 'react'
import { Button } from 'primereact/button';
import { Calendar } from 'primereact/calendar';
import { InputText } from 'primereact/inputtext';
export const App: FC = () => {
  return (
    <>
      <div className=' p-20 w-screen h-screen'>
        <InputText />
        <Button label='ทดสอบ' icon='pi pi-verified' />
        <Calendar dateFormat="dd/mm/yy" showButtonBar pt={{
          input: {
            root: () => ({
              className: "text-center"
            })
          },
        }} />
      </div>
    </>
  )
}
